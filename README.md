# imdb Front
![React](https://img.shields.io/badge/React-white?style=plastic&logo=react&logoColor=%2361DAFB&color=black)
![JavaScript](https://img.shields.io/badge/JavaScript-white?style=plastic&logo=javascript&logoColor=white&color=%23F7DF1E)

## Présentation
La partie front-end du projet de modification du [Top 250 Movies](https://www.imdb.com/chart/top/) de l'Internet Movie Database, qui permet de comparer ce classement avec une version qui ne tient pas compte des votes pour les notes extrêmes (1 et 10) dans son calcul. Les données utilisées ont été scrapées du site le 07/06/2024.
  
Pour plus de contexte, voir la [partie back-end du projet](https://gitlab.com/a_thomas_dev/imdbscrap).
  
## Résultat
[Voici la page résultante](https://imdb-front.vercel.app/), utilisant le framework React.