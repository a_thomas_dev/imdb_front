import { useEffect, useState } from "react";
import MovieCard from "./MovieCard.jsx";
import imdbNewRatings from "../imdbNewRatings.json";
import SortingButton from "./SortingButton.jsx";

export default function Classements() {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    setMovies(imdbNewRatings);
  }, []);

  const sortedByRedoneImdbWeightedRating = [...movies].sort(
    (a, b) =>
      a.redoneImdbWeightedRatingPosition - b.redoneImdbWeightedRatingPosition
  );
  const sortedByWeightedRatingSans10et1Stretched = [...movies].sort(
    (a, b) =>
      a.weightedRatingSans10et1StretchedPosition - b.weightedRatingSans10et1StretchedPosition
  );
  const sortedByredoneImdbWeightedRatingPosDiff = [...movies].sort(
    (a, b) =>
      b.redoneImdbWeightedRatingPosDiff - a.redoneImdbWeightedRatingPosDiff
  );
  const sortedByweightedRatingSans10et1StretchedPosDiff = [...movies].sort(
    (a, b) =>
      b.weightedRatingSans10et1StretchedPosDiff - a.weightedRatingSans10et1StretchedPosDiff
  );

  const [SortingByPosition1, setSortingByPosition1] = useState(true);
  const [SortingByPosition2, setSortingByPosition2] = useState(true);

  return (
    <div className="app mt-25 mb-25">
      <div className="container">
        <div className="left-column bg-white">
          <a href="https://www.imdb.com/chart/top/" rel="noopener noreferrer">
            <h2 className="classement-title mb-58 hover-grey">Classement original</h2>
          </a>
          {movies.map((movie) => (
            <MovieCard key={movie.name} movie={movie}/>
          ))}
        </div>

        <div className="middle-column bg-white">
          <h2 className="classement-title">
            Moyennes Pondérées Recalculées (MPR)
          </h2>
          <SortingButton
            onClick={() => setSortingByPosition1(!SortingByPosition1)}
            isSortingByPosition={SortingByPosition1}
          />
          {SortingByPosition1
            ? sortedByRedoneImdbWeightedRating.map((movie, index, difference) => (
                <MovieCard key={movie.name} movie={movie} index={index} difference={movie.redoneImdbWeightedRatingPosDiff}/>
              ))
            : sortedByredoneImdbWeightedRatingPosDiff.map((movie, index, difference) => (
                <MovieCard key={movie.name} movie={movie} index={index} difference={movie.redoneImdbWeightedRatingPosDiff}/>
              ))}
        </div>

        <div className="right-column bg-white">
          <h2 className="classement-title">MPR sans les notes 1 et 10</h2>
          <SortingButton
            onClick={() => setSortingByPosition2(!SortingByPosition2)}
            isSortingByPosition={SortingByPosition2}
          />
          {SortingByPosition2
            ? sortedByWeightedRatingSans10et1Stretched.map((movie, index, difference) => (
                <MovieCard key={movie.name} movie={movie} index={index} difference={movie.weightedRatingSans10et1StretchedPosDiff}/>
              ))
            : sortedByweightedRatingSans10et1StretchedPosDiff.map((movie, index, difference) => (
                <MovieCard key={movie.name} movie={movie} index={index} difference={movie.weightedRatingSans10et1StretchedPosDiff}/>
              ))}
        </div>
      </div>
    </div>
  );
}
