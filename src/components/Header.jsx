export default function Header() {
  return (
    <div className="bg-white row full-center border-bottom">
      <div className="flex1 row align-center">
        <a href="/home" rel="noopener noreferrer">
          <div className="button">Accueil</div>
        </a>
        <a href="/about" rel="noopener noreferrer  gap-1">
          <p className="button">À propos</p>
        </a>
      </div>
      <div className="flex1 row">
        <img src="imdb.png" alt="logo imdb" />
        <h1 className="main-title">TOP 250 : sans les votes extrêmes</h1>
      </div>
      <div className="flex1 row"></div>
    </div>
  );
}
