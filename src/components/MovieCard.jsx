const MovieCard = ({ movie, index, difference }) => {
  const COLOR = difference < 0 ? "red" : difference > 0 ? "green" : "black";
  const SIGN = difference > 0 ? "+" : "";
  let position = index === undefined ? movie.positionImdb : index + 1;

  return (
    <div className="movie-card">
      <a href={movie.urlMovie} rel="noopener noreferrer">
        <img
          src={movie.urlPoster}
          alt={movie.title}
          className="movie-image round-poster"
        />
      </a>
      <div className="movie-info">
        <a href={movie.urlMovie} rel="noopener noreferrer">
          <h3 className="movie-title hover-grey">
            {position}
            {index !== undefined && (
              <span style={{ color: COLOR }}>
                ({SIGN}
                {difference})
              </span>
            )}
            . {movie.name}
          </h3>
        </a>
        <p className="movie-year">
          {movie.year} {movie.duree}
        </p>
        <a href={movie.urlRatings} rel="noopener noreferrer">
          <p className="movie-rating hover-black">
            Note IMDb originelle : {movie.ratingImdb}
          </p>
        </a>
        <p className="movie-rating">
          Moyenne Pondérée Recalculée :{" "}
          {movie.redoneImdbWeightedRating.toFixed(2)}
        </p>
        <p className="movie-rating">
          MPR, recalculée sans les 1 et 10 :{" "}
          {movie.weightedRatingSans10et1Stretched.toFixed(2)}
        </p>
      </div>
    </div>
  );
};

export default MovieCard;
