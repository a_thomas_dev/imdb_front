export default function SortingButton({ onClick, isSortingByPosition }) {
  return (
    <button onClick={onClick} className="button">
      {isSortingByPosition ? "Trier par progression" : "Trier par position"}
    </button>
  );
}