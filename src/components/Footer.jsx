export default function Footer() {
  return (
    <div className="wrapper full-center border-top mt-25 bg-white">
      <p></p>
      <a
        href="https://gitlab.com/users/a_thomas_dev/projects"
        rel="noopener noreferrer"
      >
        <div className="hover-grey">A_Thomas_Dev 2024</div>
      </a>
    </div>
  );
}
