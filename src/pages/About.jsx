import Header from "../components/Header.jsx";
import Footer from "../components/Footer.jsx";

export default function About() {
  return (
    <div>
      <Header />
      <h1 className="margin-27pc mt-25">Contexte</h1>
      <p className="margin-27pc mt-15">
        L'idée pour ce projet m'est venue d'une observation que j'avais faite il
        y a bien longtemps sur le site de review d'anime Animeka : Il y a pour
        chaque anime un graphique montrant le nombre de votes pour chaque note
        possible donnés par les utilisateurs (dans un choix allant de 0 à 10).
        Un constat saute presque à chaques fois aux yeux : la plupart des
        utilisateurs veulent que leur vote influence le plus possible la moyenne
        et vont donc voter soit 0 soit 10, par exemple :
      </p>

      <div className="row full-center mt-15">
        <img
          src={process.env.PUBLIC_URL + "/animeka.png"}
          alt="exemple de votes venant du site animeka"
        />
      </div>

      <p className="margin-27pc mt-15">
        Je me suis rappelé ce fait récemment et je me suis demandé si le fameux
        Top 250 Movies de l'Internet Movie Database ne souffrirait pas un peu de
        ce phénomène, qui plus est lorsque l'on prend en compte l'influence
        probable de bots à la solde du business cinématographique. Donc,{" "}
        <b>
          qu'adviendrait-il de l'ordre de cette liste si on recalculait la
          moyenne de chaque film sans prendre en compte les votes 1 et 10 ?
        </b>
      </p>

      <h1 className="margin-27pc mt-25">Scrapy</h1>
      <p className="margin-27pc mt-15">
        Scrapy est un framework open-source écrit en Python permettant de viser
        des pages web pour en récupérer des données de façon automatisée. De
        part le faible nombre de données à scrapper, j'ai choisis d'utiliser le
        middleware Selenium avec Scrapy plutôt que d'essayer de mimiquer des
        requêtes de façon organique avec juste ce dernier -ce qui peut s'avérer
        complexe face à un site d'importance comme IMDb qui a évidemment en
        place des défenses contre les utilisateurs non-humains- pour éviter
        ainsi plus facilement les réponses bloquantes de type 403. Selenium
        permet de prendre le contrôle d'un navigateur réel pour aller sur chaque
        page à scraper, et être donc plus furtif (au détriment de la vitesse
        d'execution, mais cela est insignifiant dans le cadre de ce projet).
        J'ai utilisé le navigateur Chrome (version 126.0.6478, win64). Selenium
        a besoin d'un chromedriver de même version pour fonctionner avec, dont
        on peut trouver une liste
        <a
          href="https://googlechromelabs.github.io/chrome-for-testing/"
          rel="noopener noreferrer"
        >
          ici
        </a>
        . Les données utilisées ont été scrapée du site le 07/06/2024.
      </p>

      <h1 className="margin-27pc mt-25">Exploitation des données</h1>
      <p className="margin-27pc mt-15">
        Recalculer dans un premier temps la note des films pour vérifier la
        pertinence des données récupérées n'est pas aussi simple qu'on pourrait
        le penser : Par exemple sur la page du détails des votes du film Les
        évadés, on constate qu'il y a une note nommée "NOTE IMDb" (9.3/10) mais
        aussi plus bas une note nommée "Moyenne non-pondérée" (9.1/10). La
        moyenne non-pondérée semble être calculée simplement (somme des
        multiplications de chaque note par le nombre de vote qui lui est
        attribué, divisée par la somme du nombre total de votes), puisque je
        retrouve un chiffre presque toujours similaire une fois celui-ci arrondi
        au plus proche avec un seul chiffre après la virgule (les rares
        différences pouvant s'expliquer par le manque de précision des données
        présentée : Par exemple j'ai scrappé "1.6M" de votes pour la note 10
        mais IMDb a forcément utilisé dans son calcul un chiffre plus précis).
        En ce qui concerne la "NOTE IMDb", en enquétant on retrouve la formule
        qu'IMDb a utilisé pour calculer la moyenne pondérée, bien que la
        dernière mise à jour que j'ai trouvé remonte à 2013 au mieux (mais peu
        importe, pensais-je, puisque son évolution a été mineure jusque là ; je
        serai donc probablement capable de l'adapter au goût du jour en tatonant
        un peu) :
      </p>

      <div className="row full-center mt-15">
        <img
          src={process.env.PUBLIC_URL + "/imdb_equation.png"}
          alt="equation imdb"
        />
      </div>

      <p className="margin-27pc mt-15">
        Malheureusement cela ne semble pas être aussi simple : par exemple "Les
        évadés" a une moyenne pondérée supérieure (9.3) à sa moyenne
        non-pondérée (9.1), or j'ai tracé des courbes de cette formule de
        pondération pour plusieurs nombres totaux de votes et pour des moyennes
        allant de 1 à 10, et l'on constate que cela ne devrait pas être possible
        :
      </p>

      <div className="row full-center mt-15">
        <img
          src={process.env.PUBLIC_URL + "/imdb_chart.png"}
          alt="chart imdb"
        />
      </div>

      <p className="margin-27pc mt-15">
        Axe des ordonnées : moyenne non-pondérée. Axe des abscisse : moyenne
        pondérée Toutes les moyennes non-pondérées du Top250 sont supérieures à
        7.8. Or on constate que pour les moyennes non-pondérée allant de 8 à 10,
        la formule de pondération ne peut que faire baisser la note. Pour
        expliquer ces moyennes qui augmentent après pondération, il y a donc en
        jeu plus que cette formule. On peut supposer qu'IMDb ne prend par
        exemple pas en compte les votes des utilisateurs dont le compte est trop
        récent ou qui n'ont pas écrit d'avis sur un nombre suffisant de films,
        etc. Ne m'étant pas possible d'accéder à ces données aujourd'hui, il
        faudra donc que je fasse sans la possibilité de reproduire ces moyennes
        exactement. C'est dommage puisque le but du projet s'en trouve un peu
        altéré, mais néanmoins l'exercice reste enrichissant.
      </p>
      <Footer />
    </div>
  );
}
