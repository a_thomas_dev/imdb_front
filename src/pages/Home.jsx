import React, { Component } from "react";
import Header from "../components/Header.jsx";
import Classements from "../components/Classements.jsx";
import Footer from "../components/Footer.jsx";

export default class Home extends Component {
  state = {
    allElementsRendered: false,
  };

  componentDidMount() {
    //création d'un delay pour laisser le temps de downloader les posters des films
    setTimeout(() => {
      this.setState({ allElementsRendered: true });
    }, 100); //temps de délai (ici 100ms)
  }

  render() {
    const { allElementsRendered } = this.state;

    if (!allElementsRendered) {
      return null; //éventuellement mettre une image de loading
    }

    return (
      <div className="bg-black">
        <Header />
        <Classements />
        <Footer />
      </div>
    );
  }
}